---
# Front matter
wrapper: md_wrapper_1.html
title: Nodes
subtitle: The peers of OctoMY&trade;

pre_wrappers:
 - includes/md_head.html

post_wrappers:
 - 'includes/md_foot.html'

---

{% include 'includes/page_subpages.html' %}

{% with topics=['components', 'architecture'] %}
{% include 'includes/page_topics.html' %}
{% endwith %}


# Agent

![Agent](/static/images/nodes/Agent_logo.svg)


The OctoMY™ Agent represents a robot. It is called an Agent instead of a robot simply because Agent is more general and so does not impose any misconceptions as to what this node is capable of or designed for.

A typical Agent could be anything from an RC car fitted with an Arduino + Android combo to a hexapod robot or quad-copter.

A less typical but still very useful Agent could be a virtual robot running in a virtual world inside a simulation.

# Remote

![Remote](/static/images/nodes/Remote_logo.svg)

The OctoMY™ Remote represents the user(s) of the system. It is will allow computers and human operators alike to provide their input.

The forms in which input is given can vary from very low level such as manipulating a single actuator in real-time directly to very high-level such as staking out target points on a map or offering commands that require considerably more thinking on the part of the system.

Important concepts in the Remote is real-time access to status info through a very dynamic and adaptive UI.

	
# Hub

![Hub](/static/images/nodes/Hub_logo.svg)

The OctoMY™ Hub is a central component meant to run on relatively stationary performance hardware such as server/workstation or cloud instances.

A typical purpose of the Hub is to gather all data from remotes/agents for storage and heavy/offline processing.

Another typical purpose is coordination and facilitation between the different remotes and agents out there, like for example sensor data sharing, collision avoidance and coordination of a flock of robots.


#Zoo

![Zoo](/static/images/nodes/Zoo_logo.svg)

The OctoMY™ Zoo is a global online community where robots go to mingle and show off.
