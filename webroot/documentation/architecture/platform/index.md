---
# Front matter
wrapper: md_wrapper_1.html
title: Platform
subtitle: The platform supporting OctoMY&trade;

pre_wrappers:
 - includes/md_head.html

post_wrappers:
 - 'includes/md_foot.html'

---

{% include 'includes/page_subpages.html' %}

{% with topics=['components', 'architecture'] %}
{% include 'includes/page_topics.html' %}
{% endwith %}


## Platform

The development environment for OctoMY [based on C++/Qt5](https://www.qt.io/) including the [Qt Build System (Qbs](http://qbs.io/).

Qt comes with its own IDE called [QtCreator](https://www.qt.io/product/development-tools) which integrates all of Qt's tools. After integrating the Android development tools, QtCreator will also control the build of Android apps transparently.

![C++ Logo](/static/images/brands/ISO_C++_Logo.svg)

## Mobile

The **remote** and **agent** nodes in the project are meant to be easily run from mobile devices.

**NOTE:** I have only tested this with Android, but Qt promises support for other mobile platforms as well. Any effort to get the project running with other mobile platforms like [iOS](https://doc.qt.io/qt-6/ios.html) is [welcome](/contribute)!

## Android

Here is a ([tutorial for getting up and running with Android development in Qt](https://doc.qt.io/qt-6/android.html))


## Qt

![Qt Company Logo](/static/images/brands/Qt_logo.svg)

Qt was selected because it is a really good architectural fit for the project. I also happen to enjoy the Qt platform. It is my hope that you will find this platform enjoyable to work with too!

I wrote a post on stack overflow on why [Qt would be good for game development](https://stackoverflow.com/a/28573370/1035897). In many respects this answer also answers why Qt is a good fit for OctoMY™.

