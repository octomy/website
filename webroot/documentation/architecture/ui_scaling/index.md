---
# Front matter
wrapper: md_wrapper_1.html
title: UI Scaling
subtitle: Best practices for UI Scaling in Qt

pre_wrappers:
 - includes/md_head.html

post_wrappers:
 - 'includes/md_foot.html'

---

{% include 'includes/page_subpages.html' %}

{% with topics=['components', 'architecture'] %}
{% include 'includes/page_topics.html' %}
{% endwith %}


The user interfaces of OctoMY strive for using best practices for UI scaling. In Qt, the best practices are outlined [here](https://doc.qt.io/qt-6/highdpi.html).

In summary, we do the following:

- Use the [qreal](https://doc.qt.io/qt-6/qttypes.html#qreal-typedef) versions of the [QPainter](https://doc.qt.io/qt-6/qpainter.html) drawing API (position() instead of pos()).
- Size windows and dialogs in relation to the screen size.
- Calculate sizes in layouts from font metrics or screen size.

