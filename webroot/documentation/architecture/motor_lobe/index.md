---
# Front matter
wrapper: md_wrapper_1.html
title: Motor Lobes
subtitle: In the brain of OctoMY&trade;

pre_wrappers:
 - includes/md_head.html

post_wrappers:
 - 'includes/md_foot.html'

---

{% include 'includes/page_subpages.html' %}

{% with topics=['components', 'architecture'] %}
{% include 'includes/page_topics.html' %}
{% endwith %}


Motor Lobe is a concept that allows the actuators of a robot to be used for different things in different contexts.

Each lobe is of a specific type such as "wheeled locomotion" or "legged locomotion" and they know how to coordinate the motion of the actuators under their command. The lobes also have their own special purpose user interfaces that makes controlling this for the user easy, for example by displaying a steering wheel.

Further, by assigning the same actuator to multiple lobes, the operator may choose how that actuator should be used. For example, a hexapod lobe will allow the operator to effortlessly coordinate the motion of 18 actuators concurrently, but by assigning the actuators of one of the legs as a "robot arm" lobe as well, the operator can switch to controlling that arm in a completely different way.

By combining actuators and lobes creatively, new and previously impossible constellations of robots can be created, such as quad-copters with legs, transforming bipeds with wheels on their feet and knees etc.

![Motor Lobes](/static/images/illustrations/Revenge_of_the_Fallen_Sideswipe.jpeg)
