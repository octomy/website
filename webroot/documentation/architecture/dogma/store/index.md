---
# Front matter
wrapper: md_wrapper_1.html
title: Dogma - Store
subtitle: OctoMY&trade; dogma

pre_wrappers:
 - includes/md_head.html

post_wrappers:
 - 'includes/md_foot.html'

---

{% include 'includes/page_subpages.html' %}

{% with topics=['dogma', 'architecture'] %}
{% include 'includes/page_topics.html' %}
{% endwith %}


**Draft**

The run-time data of each OctoMY™ node is kept in a store. The store is good at keeping relatively small amounts of structured data in memory for easy and quick access.

The store is really a QVariantMap tree with wrappers to manipulate the data asynchronously.

The store will take care of loading at startup and persisting on changes and shutdown.

Some data-items in the store are backed by generators which means that if they don't exist at the time of access, the generator will be invoked to provide them.

Persistence of the store can be carried out over multiple files. This allows keeping one store in memory but multiple files on disk.

The API consists of a very small and simple interface that allows user to not care about the complexity in the levels below. The interface looks like this:

- `get(Key key, Callback(Status status, Data *data))`
- `put(Key key, Data *data, Mode mode)`
- `key.op(cb(opstat), opdef)`
- `ctx.op().cb((op op){}).mode(sync)`

Where Key is an identifier for the data you would like to access, callback is the function that will be called upon completion (async), Status is the state in which the get call ended, Data is the actual data to get/put or null to signify no data and Mode is the desired mode in which you wish to put the data.

At the highest level, the user will call this interface to transmit data to and from storage.

At intermediate levels of the stack, the interface may be implemented to do all kinds of operations such as loading data from disk, muxing calls based on key, generating data based on key, or forwarding access over the network, etc. The point is the interface hides all this activity from the user and allows multiple components to be stacked using the same interface.

**Key**

The key is used to look up a value in the store. It is similar to a bare bones [XPath](https://en.wikipedia.org/wiki/XPath), just not tied to XML. The key can be represented as a simple string that is a set of node specifiers concatenated with separators.

An example key could look like this:

`keystore.localkey.pub`


This accesses the public part of the local key in the keystore.


`keystore.remotekey[CAFEBABE80085]`

