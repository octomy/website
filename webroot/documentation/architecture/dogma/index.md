---
# Front matter
wrapper: md_wrapper_1.html
title: Dogma
subtitle: OctoMY&trade; dogma

pre_wrappers:
 - includes/md_head.html

post_wrappers:
 - 'includes/md_foot.html'

---

{% include 'includes/page_subpages.html' %}

{% with topics=['dogma', 'architecture'] %}
{% include 'includes/page_topics.html' %}
{% endwith %}

Here's the markdown version of the provided HTML snippet:


### Contents

 - [Condition syntax](/documentation/architecture/dogma/condition_syntax)
 - [Section syntax](/documentation/architecture/dogma/section_syntax)
 - [Skeptron](/documentation/architecture/dogma/skeptron)
 - [Store](/documentation/architecture/dogma/store)

# Introduction

Dogma is a declarative translation unit that describes all aspects of a node. It is written in a special json based schema that the node can interpret to set itself up with the correct configuration.

The configuration covers the following areas:

 - Triggers and Actions
 - Directives
 - Description of locations and areas in a map
 - Reference to other nodes by ID
 - Grouping tags and node selectors
 - References to web services by URL
 - Shared secrets
 - Hardware configuration
 - Lobe configuration
 - And much much more!

# Structure

The file contains one root element with the following parts:

 - Zero or more detail sections describing different parts of the node such as references, lobes, secrets and poses
 - Exactly one directive section marked "main" that may reference the detail sections
 - Zero or more secondary directive sections that may reference the detail sections

The main directive section must contain the ID of the node which matches the key-pair in the key-store for the node. If this has not been specified correctly the robot will enter a vegetative safety state and will not respond to any commands.

# References & inlining

Detail sections may be inlined directly where they are used or they may be referenced by a hashtag followed by their unique name.

### Example of in-lining:

```json
"directive": {
    "name": "A113",
    "description": "Dispose of the plant",
    "secret": {"text": "THIS IS THE SECRET DON'T TELL ANYONE!!"}
}
```

### Example of reference:

```json
"secret": {
    "name": "A113SECRET",
    "text": "THIS IS THE SECRET DON'T TELL ANYONE!!"
}
```

```json
"directive": {
    "name": "A113",
    "description": "Dispose of the plant",
    "secret": "#A113SECRET"
}
```

The benefit of in-lining is obviously that it for simple cases saves a lot of lines. The benefit of references is that several sections may reference the same secondary section, making for less copy-pasting and sources of error. Also if many reference it there is a a possibility of considerable savings in number of lines.
