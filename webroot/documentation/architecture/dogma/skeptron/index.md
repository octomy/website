---
# Front matter
wrapper: md_wrapper_1.html
title: Dogma - Skeptron
subtitle: OctoMY&trade; dogma

pre_wrappers:
 - includes/md_head.html

post_wrappers:
 - 'includes/md_foot.html'

---

{% include 'includes/page_subpages.html' %}

{% with topics=['dogma', 'architecture'] %}
{% include 'includes/page_topics.html' %}
{% endwith %}

# Skeptron

The node that owns the dogma is called the *skeptron*, and it must be trusted by all nodes mentioned in the dogma for it to become effective. In other words, each peer may decide on its own if it wants to accept the dogma, and usually dogma are only accepted when coming from trusted nodes.

## Node description

The internal description that each node has of themselves can be either:

- **Auto detected**: The parts that each node is able to automatically discover about themselves by enumerating and probing their hardware.
- **Specified**: The part that needs to be specified for each node, typically in UI during initial setup.

Many essential features such as GPS, cameras, and network capabilities are auto-detectable, while more exotic features such as access to specific online resources (for hubs) or the availability of certain attached controllers or extra sensors (for agent) must be done manually. The manually specified features are given through sections of the dogma.

