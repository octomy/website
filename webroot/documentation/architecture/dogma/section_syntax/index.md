---
# Front matter
wrapper: md_wrapper_1.html
title: Dogma - Section Syntax
subtitle: OctoMY&trade; dogma

pre_wrappers:
 - includes/md_head.html

post_wrappers:
 - 'includes/md_foot.html'

---

{% include 'includes/page_subpages.html' %}

{% with topics=['dogma', 'architecture'] %}
{% include 'includes/page_topics.html' %}
{% endwith %}

### Common section anatomy

The following fields are possible to put in any section:

- **From** - A reference to another "base" section from which this section will inherit its field values. If this is missing, the section will automatically inherit from the "default" section of this type which contains the default values of such a section. Circular reference loops are not allowed.
- **Name** - Optional name of the section. This is necessary if you want to reference the section. It is not necessary if you "inline" the section into another. Name must be unique for all sections regardless of type in the entire dogma translation unit.
- **Description** - Optional description of the section. This is only for documentation purposes. Description will be propagated to UI so that it becomes visible either as a label or as a tool-tip for reference by the user.

### Directive

- **Personality** - An integer index that is added to the personality PRNG seed to vary the personality. For a glimpse into personality, see [this](https://blog.octomy.org/2016/08/octomy-identicons.html) and [this](https://blog.octomy.org/2017/08/the-eyes-are-mirror-of-soul.html).
- **Name** - The given name that this node will go by with her friends.
- **Gender** - The gender identity of this node.

### Controller

- **Type** - The controller type. One in [ardumy, servotor32]
- **Controller specific fields** - The rest of the section will be specific to the controller type that was selected

#### ArduMY Controller

- **Actuators** - List of actuators

#### Servotor32 Controller

- **Actuators** - List of actuators

### Actuator reference

- **Controller** - The controller which contains the actuator
- **Actuator** - The actuator
- **Flags** - Active flags in [reverse]

### Lobe

- **Type** - The lobe type in [tracked, wheeled, hovering, legged]
- **Lobe specific fields** - The rest of the section will be specific to the lobe type that was selected (see lobe specific sections below)

#### Wheeled lobe

Explanation is [here](https://blog.octomy.org/2016/12/two-new-widgets.html).

- **Throttle** - Actuator reference to use for throttle
- **Steering** - Actuator reference to use for steering

#### Leg lobe

Explanation is [here](https://blog.octomy.org/2013/08/robot-leg-anatomy.html).

- **Hip (Coxa)** - Actuator reference to use for hip
- **Thigh (Femur)** - Actuator reference to use for thigh
- **Shin (Tibia)** - Actuator reference to use for shin
- **Foot (Tarsus)** - Optional actuator reference to use for foot

### Secret

- **Text** - The secret string in clear-text

### Gear ratio

- **Gear-ratio-numerator** - The gear numerator part of the ratio of the actuator
- **Gear-ratio-denominator** - The gear denominator part of the ratio of the actuator

### Actuator

- **Type** - in [RC-Servo, step-motor, brush-less-dc, brushed-dc]
- **Data-bits** - int number of bits
- **Data-type** - in [float, fixed, int]
- **Range-lower** - The lower value possible for this actuator
- **Range-higher** - The higher value possible for this actuator
- **Limit-lower-enable** - Enable for lower limit switch
- **Limit-higher-enable** - Enable for higher limit switch
- **Limit-lower-pin** - The pin for lower limit switch
- **Limit-higher-pin** - The pin for higher limit switch
- **Position-feedback-enable** - Enable for position feedback
- **Position-feedback-pin** - The pin for position feedback
- **Tachometer-enable** - Enable for tachometer
- **Tachometer-pin** - The pin for tachometer pulse
- **Encode-enable** - Enable for encoder
- **Encode-absolute-enable** - Enable for absolute vs. relative encoder
- **Encode-a-pin** - The A pin for encoder
- **Encode-b-pin** - The B pin for encoder
- **Gear ratio** - The gear ratio
- **Flags** - Flags for the actuator in [continuous, linear, reversed]

### Puppet

- **Actuators** - List of puppet actuators

### Pose

- **Puppet** - Reference to the puppet
- **Values** - List of puppet actuators by ID with:
	- Actuator mode in [limp, freeze, train, move]
	- Actuator value (only used if mode is move or train)

### Animation

- **Poses** - List of poses, each with a nanosecond time stamp relative to animation start. Only poses for one puppet are allowed.

### Condition

- **Text** - The condition statement in condition syntax

### Trigger

- **Conditions** - List of references to conditions that need to evaluate to true for the trigger to activate
- **Actions** - List of references to directives to invoke in order when the trigger is activated

The expression in the condition follows a simple syntax outlined [here](/documentation/architecture/dogma/condition_syntax).

### Resource

- **URI** - The location of the resource
