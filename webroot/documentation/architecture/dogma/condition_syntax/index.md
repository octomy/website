---
# Front matter
wrapper: md_wrapper_1.html
title: Dogma - Condition Syntax
subtitle: OctoMY&trade; dogma

pre_wrappers:
 - includes/md_head.html

post_wrappers:
 - 'includes/md_foot.html'

---

{% include 'includes/page_subpages.html' %}

{% with topics=['dogma', 'architecture'] %}
{% include 'includes/page_topics.html' %}
{% endwith %}

## Condition Syntax

Condition syntax is the language used for [conditions](/stati/images/not_found.svg).

The following formulas can be arranged into expressions with parentheses and boolean operators, where the <PLACEHOLDERS> represent selectors or values (see right column for details).

```python
<NODE> enters location <LOCATION>
<NODE> is in location <LOCATION>
<NODE> exits location <LOCATION>

<NODE> enters view <CAMERA>
<NODE> is in view <CAMERA>
<NODE> exits view <CAMERA>

<PERSON> enters view <CAMERA>
<PERSON> is in view <CAMERA>
<PERSON> exits view <CAMERA>

unidentified person enters view <CAMERA>
unidentified person exits view <CAMERA>

<LANDMARK> enters view <CAMERA>
<LANDMARK> exits view <CAMERA>

<NODE> enters comms range
<NODE> is in comms range
<NODE> exits comms range

time since last seen <NODE> <COMPARATOR> <COUNT> <TIME-INTERVAL-UNIT>
time since last seen <LANDMARK> <COMPARATOR> <COUNT> <TIME-INTERVAL-UNIT>
time since last seen <PERSON> <COMPARATOR> <COUNT> <TIME-INTERVAL-UNIT>

<SENSOR> value becomes <COMPARATOR> <FLOAT>
<SENSOR> value is <COMPARATOR> <FLOAT>

<SENSOR> value enters range <FLOAT> to <FLOAT>
<SENSOR> value is in range <FLOAT> to <FLOAT>
```

## Expressions

Expressions are formed when condition formulas are combined with the following operators:

- `and`
- `or`
- `(`
- `)`

Examples:

- `default.me enters location #garden and #lennart is in #garden`
- `( default.me enters location #garden and #lennart is in #garden) or #other_robot enters comms range`

### Selectors

Selectors will match zero or more items of the type specified. For example, `<CAMERA>` could be `"#mycamera"`, `"#mycam1,#mycam2"` or `*` to denote a specific camera, a list of specific cameras, or any camera respectively.

If there is a typo or the item you want does not exist, the selector will return 0 items and disable the condition it was part of.

### Types

The following items can be selected:

- Node
- Person
- Landmark
- Location
- Sensor

### Name

Examples:

- `"#golem"` - Select node identified by name
- `"#kitchen"` - Select landmark identified by name
- `"#lennart"` - Select person identified by nickname
- `"#mycam"` - Select camera identified by name
- `"#mythermometer"` - Select thermometer sensor identified by name

### ID

Example:

- `"#57234i747293847923487"` - Select item identified by automatically assigned unique ID

### List

Example:

- `"#mycam1, #mycam2"` - Select camera identified by either of two specified names

### Wildcard

Examples:

- `*` - Select all/any item

### Default

Examples:

- `"default.me"` - Select local (ourself) node (node)
- `"default.front"` - Select the default front-facing camera (camera)
- `"default.back"` - Select the default back-facing camera (camera)
- `"default.temp"` - Select the default thermometer (sensor)

## Values

Values are specified in placeholders just like selectors, but they represent literal values or enums.

### Count

Valid values are all unsigned integers up to 2^64.

### Time Interval Unit

Valid values are:

- **Y** - Years
- **M** - Months
- **W** - Weeks
- **D** - Days
- **h** - Hours
- **m** - Minutes
- **s** - Seconds
- **ms** - Milliseconds
- **u** - Microseconds
- **n** - Nanoseconds

### Comparator

Valid values are:

- **>** - Greater than
- **<** - Less than
- **>=** - Greater than or equal
- **<=** - Less than or equal
- **==** - Equal
- **!=** - Not equal
