---
# Front matter
wrapper: md_wrapper_1.html
title: ArduMY
subtitle: How we communicate with Arduino

pre_wrappers:
 - includes/md_head.html

post_wrappers:
 - 'includes/md_foot.html'

---



{% include 'includes/page_subpages.html' %}

{% with topics=['communications', 'architecture'] %}
{% include 'includes/page_topics.html' %}
{% endwith %}



ArduMY™ is a protocol designed to communicate robot actuator configuration and state from OctoMY™ to Arduino® over a serial link, either wired ([D-SUB](https://en.wikipedia.org/wiki/D-subminiature#Communications_ports)/[USB](https://en.wikipedia.org/wiki/USB_communications_device_class)) or wireless ([Bluetooth](https://en.wikipedia.org/wiki/List_of_Bluetooth_profiles#Serial_Port_Profile_.28SPP.29)).

While ArduMY™ is (as the name suggests) geared towards Arduno, it was made in a way that would make it easily adaptable to other MCU/SOC devices.

![ArduMY™ Logo](/static/images/brands/ArduMY.svg)





The main features of the protocol are as follows:

- **Nimble:** It should use as little data as possible both in transfer as well as in memory.
- **Efficient:** Don't tax any CPUs or MCUs unnecessarily.
- **Portable:** The ArduMY™ is a sub-project that can be used outside OctoMY, and does *not rely on Qt5*.
- **Flexible:** The protocol supports a lot of different actuators with a lot of optional features out of the box, and adding new actuator types, as well as new optional features should be relatively easy.
- **Compatible:** ArduMY™ should run on about any Arduino® compatible device, be it small or big, expensive or cheap, real or clone. It is also expected that the protocol will be backwards compatible with older versions of the protocol, and that when interfacing devices with different ArduMY™ protocol versions, in the worst case we will simply miss the new features added to the new version.
- **Robust:** The [test-code](https://github.com/mrdeveloperdude/OctoMY/tree/master/test/testArduMY) for ArduMY™ is several times larger than the [implementation itself](https://github.com/mrdeveloperdude/OctoMY/tree/master/libs/arduino) and strives for 100% coverage by unit tests, partial-integration tests, black-box tests, fuzzing tests and finally stress tests. This means that any adverse effects from changes to the code should be picked up easily and fixed.

## Protocol Commands

**CommandSerializer** and **CommandParser** are at the top level. They are responsible for the main flow of commands from application (serializer) to controller (parser). The 1-byte commands are:

| Command                         | Description |
|---------------------------------|-------------|
| **OCTOMY_SYNC**                 | Synchronize in the case that serial link failed and dropped one or more bytes. Done at interval and at error detected to keep the link healthy. |
| **OCTOMY_AWAITING_COMMAND**     | Ready to accept command. |
| **OCTOMY_SET_ACTUATOR_COUNT**   | The actuator count will change. The next byte is the new actuator count (0-255). *NOTE: It is expected that when the number of actuators decrease, the remaining actuators will retain their previous configuration.* *NOTE: It is expected that when the number of actuators increase, the new actuators will be reset to default configuration.* |
| **OCTOMY_SET_ACTUATOR_CONFIG**  | The actuator configuration will change for one actuator. The next byte will identify the actuator (0-255), and its entire configuration data will follow. |
| **OCTOMY_SET_ACTUATOR_VALUES**  | The value of one or more actuators will change. The following 1 to 32 bytes (8 to 256 bits) is a bitmap that identifies which of the actuators that are marked for change, followed by the change for each actuator in batches per representation. |
| Reserved for future             | New commands may be added to this list in later versions. |

It is understood that the application is always responsible for communication. This means that the application cannot make any assumptions about what the controller thinks, and so the application will build up all data in the controller at each start, and after each subsequent failure/interval to make sure that the application can know the current state of the controller with high certainty.

Building up of data in the controller from the application follows these steps:
1. Serial synchronization to make sure serial communication is stable.
2. Set actuator count
3. Set configuration for all actuators one by one
4. Set value of all actuators one by one

## Actuator Configuration

The actuator config is very flexible. It will support the following settings:

- Different actuator base types:
	- RC-Servo
	- Step motor
	- DC-Motor
	- Relay
- Different representations for actuator value:
	- **bit:** typically for relay on/off, position left/right etc, saves a lot of bandwidth!
	- **byte:** 8-bit (in non-continuous, typically for coarse positioning, or tri-state).
	- **word:** 16-bit (balance between data size and precision)
	- **double-word:** 32-bit (high precision)
	- **quad-word:** 64-bit (crazy precision)
	- **float:** 32-bit (high precision, high range)
	- **double:** 64-bit (crazy precision, crazy range)
- Different limitation:
	- Continuous (no end stops) like motors for a wheeled robot.
	- Non-continuous (with end stops) like RC servos that move back and forth.
- Different motion:
	- Linear motion as in hydraulic piston moving in and out.
	- Rotary motion as in horned RC servo that rotate.
- Reverse to flip direction so left is right, and forward is backward etc.
![RC-Servo settings for ArduMY™ Actuator config](https://www.octomy.org/_/rsrc/1484847044336/documentation/development/architectual-overview/ardumy/Selection_142.png)
- Trims to calibrate range of motion.
- Limit switches for the ends of the range (only for non-continuous actuators).
![Range input and limit switches for ArduMY™ Actuator config](https://www.octomy.org/_/rsrc/1484847019653/documentation/development/architectual-overview/ardumy/Selection_137.png)
- Analogue position feedback.
![Position feedback settings for ArduMY™ Actuator config](https://www.octomy.org/_/rsrc/1484847029755/documentation/development/architectual-overview/ardumy/Selection_139.png)
- Tachometer to sense speed (usually for continuous actuators only).
![Tachometer settings for ArduMY™ Actuator config](https://www.octomy.org/_/rsrc/1484847034475/documentation/development/architectual-overview/ardumy/Selection_140.png)
- Encoder to sense incremental or absolute position.
![Encoder settings for ArduMY™ Actuator config](https://www.octomy.org/_/rsrc/1484847025412/documentation/development/architectual-overview/ardumy/Selection_138.png)
- Different gear ratios.
![Gear Ratio settings for ArduMY™ Actuator config](https://www.octomy.org/_/rsrc/1484847039256/documentation/development/architectual-overview/ardumy/Selection_141.png)
- Trims to calibrate range of motion.
- Limit switches for the ends of the range (only for non-continuous actuators).
- Default (safe) position that the controller will fall back to on communication error.
- Analogue position feedback.
- Tachometer to sense speed (usually for continuous actuators only).
- Encoder to sense incremental or absolute position.
- Different gear ratios.
