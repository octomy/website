---
# Front matter
wrapper: md_wrapper_1.html
title: Project Structure
subtitle: The structure of OctoMY&trade; project

pre_wrappers:
 - includes/md_head.html

post_wrappers:
 - 'includes/md_foot.html'

---

{% include 'includes/page_subpages.html' %}

{% with topics=['components', 'architecture'] %}
{% include 'includes/page_topics.html' %}
{% endwith %}


## Folders

This minimalistic overview of the project will serve until a more comprehensive documentation is available:


<pre>
.
├── content
│   ├── design
│   │   ├── icons.svg
│   │   └── ux.svg
│   ├── doc
│   │   ├── doc.pro
│   │   ├── LICENSE_TEMPLATE
│   │   ├── ocomy.qdocconf
│   │   ├── script.js
│   │   └── style.css
│   └── website
│       ├── content.html.mustache
│       ├── footer.html.mustache
│       ├── header.html.mustache
│       └── index.html.mustache
├── integration
│   ├── deb
│   │   └── make_deb.sh
│   ├── docker
│   │   ├── build.sh
│   │   ├── clean.sh
│   │   └── qmake_test
│   │       ├── main.cpp
│   │       └── QmakeTest.pro
│   ├── local
│   │   └── tool.sh
│   ├── qbs
│   │   └── imports
│   │       ├── AppProbe.qbs
│   │       ├── App.qbs
│   │       ├── BaseProbe.qbs
│   │       ├── Files.qbs
│   │       ├── LibProbe.qbs
│   │       ├── Lib.qbs
│   │       ├── QtDepends.qbs
│   │       ├── TestProbe.qbs
│   │       ├── Test.qbs
│   │       └── utils.js
│   └── qdoc
│       └── images
├── LICENSE
├── MANDATE
├── OctoMY.qbs
├── probes.qbs
├── README.md
├── src
│   ├── agent
│   │   ├── AgentMain.cpp
│   │   ├── AgentMain.hpp
│   │   ├── app.qbs
│   │   └── README.md
│   ├── apps.qbs
│   ├── hub
│   │   ├── app.qbs
│   │   ├── HubMain.cpp
│   │   └── HubMain.hpp
│   ├── index.cpp
│   ├── libs
│   │   ├── libagent
│   │   ├── libagentclient
│   │   ├── libapp
│   │   ├── libardumy
│   │   ├── libaudio
│   │   ├── libclient
│   │   ├── libclt
│   │   ├── libcombined
│   │   ├── libcomms
│   │   ├── libdebug
│   │   ├── libglt
│   │   ├── libgps
│   │   ├── libhardware
│   │   ├── libhub
│   │   ├── libhubclient
│   │   ├── libhud
│   │   ├── libid
│   │   ├── libmap
│   │   ├── libmarkdown
│   │   ├── libmbedtls
│   │   ├── libmotorics
│   │   ├── libnode
│   │   ├── libpair
│   │   ├── libparser
│   │   ├── libplan
│   │   ├── libplot
│   │   ├── libpuppet
│   │   ├── libqpolarssl
│   │   ├── libqr
│   │   ├── libremote
│   │   ├── libremoteclient
│   │   ├── libres
│   │   ├── librng
│   │   ├── libsec
│   │   ├── libservice
│   │   ├── libstore
│   │   ├── libstyle
│   │   ├── libtest
│   │   ├── libuptime
│   │   ├── libutil
│   │   ├── libvideo
│   │   ├── libvoice
│   │   ├── libweb
│   │   ├── libwidgets
│   │   ├── libzbar
│   │   ├── libzoo
│   │   ├── libzooclient
│   │   └── README.md
│   ├── libs.qbs
│   ├── moduleheader.hpp
│   ├── OctoMY.qdocconf
│   ├── README.md
│   ├── remote
│   │   ├── app.qbs
│   │   ├── README.md
│   │   ├── RemoteMain.cpp
│   │   ├── RemoteMain.hpp
│   │   └── remote.pro
│   ├── testapp
│   │   ├── app.qbs
│   │   ├── TestApp.cpp
│   │   └── TestApp.hpp
│   └── zoo
│       ├── app.qbs
│       ├── ZooMain.cpp
│       ├── ZooMain.hpp
│       └── zoo.pro
├── test
│   ├── new_test.sh
│   ├── templates
│   │   ├── profileTemplate
│   │   │   ├── ProfileTemplate.cpp
│   │   │   ├── ProfileTemplate.hpp
│   │   │   └── profileTemplate.pro
│   │   ├── stressTemplate
│   │   │   ├── StressTemplate.cpp
│   │   │   ├── StressTemplate.hpp
│   │   │   └── stressTemplate.pro
│   │   └── testTemplate
│   │       ├── TestTemplate.cpp
│   │       ├── TestTemplate.hpp
│   │       └── testTemplate.pro
│   ├── test* Each test is in it's own test* folder here




</pre>


- **libs** - The code for all functionality of the project resides here. This is a sub-project that builds to a library that will be included by the other projects.
- **agent** - The main project for agent that links to lib and acts as the on-board controlling software for the robot.
- **remote** - The main project for remote that links to lib and acts as a dumb remote control UI. Can either be connected directly to the robot (agent) or via server.
- **hub** - The main project for hub that links to lib and acts as a more advanced remote control with UI that takes advantage of larger screens.
- **zoo** - The main project for zoo that links to lib and acts as the server-side peer (off-robot compute).
- **test** - All unit & integration tests have their own sub-folder under here with main files that link to lib and uses Qt testlib to scrutinize the code in various ways.

## Peers

The project contains 3 main peers:

- **Agent:** ("The robot") Software that runs on board a robot (such as a mobile phone). The purpose of this software is to directly manipulate the actuators and to read the sensors of the robot. Depending on the mode, agent may control the robot on its own (autonomous mode), take direct commands from a remote (direct remote mode), or take commands from a hub (indirect remote mode).
- **Hub:** ("The brain") Software that runs in the cloud somewhere, most likely on your desktop computer. The purpose of the hub is to provide the features that require heavier computing resources such as real-time processing of images for the SLAM algorithm, but also to provide a central hub for many robots to share their data when collaborating in flocks or similar.
- **Remote:** ("The control") Software that runs on a remote control device (such as a mobile phone). The purpose of this software is to take input from the user on how a robot or hub should operate while displaying real-time data from the robots/hub.

Since all peers are written in the same code base, it is easy to move or duplicate features between them. For example, if you have sufficiently powerful hardware on board the robot to do SLAM there, simply invoke SLAM from the agent directly.

