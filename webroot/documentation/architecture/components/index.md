---
# Front matter
wrapper: md_wrapper_1.html
title: Components
subtitle: The components of OctoMY&trade;

pre_wrappers:
 - includes/md_head.html

post_wrappers:
 - 'includes/md_foot.html'

---

{% include 'includes/page_subpages.html' %}

{% with topics=['components', 'architecture'] %}
{% include 'includes/page_topics.html' %}
{% endwith %}

![OctoMY&trade; Topology](/static/images/illustrations/topology.svg)


This illustration shows the main components of the distributed solution that is OctoMY™

There are sensors, audio and video inputs. Their values are recorded in real-time in a local event inventory which is basically a very performant and scaleable time series database.

Some of the data is pre-processed using different filters such as speech recognition, object detectors or SLAM to generate higher level events.

There is a logic inference process running asynchronously in the background that is constantly augmenting the events in the local event inventory using common sense databases.

The master consciousness driver is the main process of the system. It controls the operation of all the other systems. When the system is running normally, it will process the most recent events from the local event inventory and make immediate decisions for what is the next behavior to conduct. It in turn generates high level instructions to route planner which in turn control the robot hardware and to the agenda planner which manages and refines agendas for future actions.

The actuator control sends signals to actuators based on the currently executing behaviors while video and audio outputs show status and user interface.

If the agent is in contact with hub, the local event inventory may be shared, queried or augmented by the global event inventory. The hub may also have one or more asynchronous background processes that run their own consciousness drivers that have access to other resources such as compute and online services and databases.
