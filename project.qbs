Project {
    name: "octomy/website"
    Product {
        Group{
            name: "Code"
            prefix: "octomy"
            excludeFiles: "/**/internal/**/*"
            files: [
                "/**/*.html",
                "/**/*.jinja2",
                "/**/*.py",
                "/**/*.txt",
            ]
        }
        Group{
            name: "Webroot"
            prefix: "webroot"
            excludeFiles: "/**/internal/**/*"
            files: [
                "/**/*.html",
                "/**/*.md",
                "/**/*.jinja2",
                "/**/*.jpeg",
                "/**/*.json",
                "/**/*.png",
                "/**/*.svg",
            ]
        }
        Group{
            name: "Meta"
            prefix: "./"
            excludeFiles: "/**/internal/**/*"
            files: [
                // Project files
                "*.qbs",
                // Environment
                ".env",
                // Ignore files for docker context
                ".gitignore",
                // Ignore files for git staging
                ".dockerignore",
                // Gitlab files such as CI/CD yaml
                ".gitlab/*",
                // Changelog detailing development history of this project
                "CHANGELOG",
                // The dockerfile to build image for this project (needs to be in the root)
                "Dockerfile",
                // Make file with dependencies
                "Makefile*",
                // README generated from template
                "README.md",
                // Version source
                "VERSION",
                // License
                "LICENSE",
                // Resource files
                "resources/*",
                // Configuration files
                "config/*.yaml",
                // Docker & compose files
                "Dockerfile",
                "docker-*.yaml",
                "docker/*.yaml",
                "docker/Dockerfile*",
                // Local helpers to load virtual environment and environment variables
                "local-*.sh",
                //Entrypoint
                "entrypoint.sh",
                // Main project file for IDE
                "project.qbs",
                // Python package requirements
                "requirements/*.in",
                "requirements/*.txt",
                // Python package configuration
                "*setup.*",
                // README template
                "resources/templates/README.md",
            ]
            
            
        }
        Group{
            name: "Bootstrap"
            prefix: "bootstrap"
            excludeFiles: "/**/internal/**/*"
            files: [
                "main.scss",
            ]
        }
    
    }
}

