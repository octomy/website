[![pipeline  status](https://gitlab.com/octomy/website/badges/production/pipeline.svg)](https://gitlab.com/octomy/website/-/commits/production)

<!---
                                                         
                                                         
     ## ## ## ## ## ## ## ## ## ## ##                    
        ## ## ## ## ## ## ## ## ##                       
           ## ## ## ## ## ## ##                          
              ## ## ## ## ##                             
                 ## ## ##                                
                    ##                                   
                                                         
                                                         
WARNING: This file is AUTO GENERATED from "".
         Any changes you make will be OVERWRITTEN at the 
         next invocation of relevant `make` target.      
                                                         
                                                         
                    ##                                   
                 ## ## ##                                
              ## ## ## ## ##                             
           ## ## ## ## ## ## ##                          
        ## ## ## ## ## ## ## ## ##                       
     ## ## ## ## ## ## ## ## ## ## ##                    
                                                         
                                                         
-->

#  Practical details
<img  src="https://gitlab.com/octomy/website/-/raw/development/resources/images/logos/1024px.svg?ref_type=heads&amp;inline=false"  width="20%"/>

This is the website project version 0.0.1

- The website should be [live here](https://octomy.org).
- website is [available  on  gitlab](https://gitlab.com/octomy/website).
- website is [available as private Docker image](https://gitlab.com/octomy/website/container_registry).


```shell
#  Clone  git  repository
git  clone  git@gitlab.com:octomy/website.git
```

```shell
# Pull image from Docker registry
docker pull registry.gitlab.com/octomy/website

```


# What is website?

website is the official website of the OctoMY&trade; project. 


What goals/features does website have?
------------------------------------------

1. Not Wordpress, Hugo, Google Sites.
2. Flexible to add whatever APIs and integrations we need.
3. Running on unified platform based on Python/FastAPI/PostgreSQL/Docker
