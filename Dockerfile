# syntax = docker/dockerfile:1.0-experimental
FROM registry.gitlab.com/octomy/base-py:3.11.0


ENV PATH="$VIRTUAL_ENV/bin:$HOME/.local/bin:${PATH}"
ARG FK_PYPI_TOKEN
ENV FK_PYPI_TOKEN=$FK_PYPI_TOKEN

# Enable gitlab private pypi package registry
ENV PIP_EXTRA_INDEX_URL https://__token__:${FK_PYPI_TOKEN}@gitlab.com/api/v4/projects/23170380/packages/pypi/simple

# Speed up build by putting a requirements install step in its own layer before the rest
COPY --chown=8888:8888 requirements/ ./requirements
RUN pip install --no-cache-dir -r ./requirements/requirements.txt

COPY --chown=8888:8888 README.md VERSION LICENSE CHANGELOG ./
# Data driven configuration defaults (will be overridden by local_config.json and environment variables in that order)
#COPY --chown=8888:8888 config.json ./
COPY --chown=8888:8888 setup.cfg setup.py ./
COPY --chown=8888:8888 octomy ./octomy
COPY --chown=8888:8888 webroot ./webroot
# Data driven configuration
COPY --chown=8888:8888 config ./config
RUN ls -halt
#RUN echo "FK_PYPI_TOKEN=${FK_PYPI_TOKEN}"
#RUN echo "PIP_EXTRA_INDEX_URL=${PIP_EXTRA_INDEX_URL}"
RUN pip install -e ./
RUN pip freeze -v


ENTRYPOINT ["uvicorn", "octomy.website.main:app"]
CMD [ "--host", "0.0.0.0", "--port", "80"]
