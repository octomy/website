from contextlib import contextmanager
import sys



@contextmanager
def custom_redirection(fileobj):
	old = sys.stdout
	sys.stdout = fileobj
	try:
		yield fileobj
	finally:
		sys.stdout = old
