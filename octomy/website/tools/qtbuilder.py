#!/bin/env python3
from octomy.log import setup_logging

import datetime
import docker
import hashlib
import jinja2
import json
import os
import pathlib
import pprint
import re
import signal
import time


logger = setup_logging(__name__)

TOOL_NAME = "OctoMY™ static-qt"
IMAGE_NAME = "static-qt"
FINGERPRINT_CONTAINER_PREFIX = "fp-"
REGISTRY_DOMAIN = f"registry.gitlab.com"
REGISTRY_NAME = f"{REGISTRY_DOMAIN}/octomy/{IMAGE_NAME}"
ID_PREFIX = "sha256:"
KILL_TIMEOUT = 10
ROOT_DIR= f"{os.path.dirname(__file__)}"
TEMPLATE_DIR= f"{ROOT_DIR}/templates"
TEMPLATE_FILE = f"{TEMPLATE_DIR}/Dockerfile.jinja2"
FROM_FILE = f"{TEMPLATE_DIR}/docker-from.jinja2"
PULL_FILE = f"{TEMPLATE_DIR}/docker-pull.jinja2"
DOWNLOAD_PKG_FILE = f"{TEMPLATE_DIR}/download-os-pkg.jinja2"
VARIATIONS_DIR = f"{ROOT_DIR}/variations"
_client = None

no_candidate_re=re.compile("^E: Package '(.*)' has no installation candidate$", flags=re.M)
not_found_re=re.compile("^E: Unable to locate package (.*)\s*$", re.M)


version_re_flags = re.M
version_re = {
	  "os_dist":re.compile("^ID=([a-z0-9]*)$", version_re_flags)
	, "os_version":re.compile("^VERSION_CODENAME=([a-z0-9]*)$", version_re_flags)
	, "make":re.compile("^GNU Make ([0-9\.]*)$", version_re_flags)
	, "git":re.compile("^git version ([0-9\.]*)$", version_re_flags)
	, "gpp":re.compile("^g\+\+\s*\([^\)]*\)\s*([0-9\.]*)\s*", version_re_flags)
	, "python":re.compile("^Python ([0-9\.]*)", version_re_flags)
	, "gdb":re.compile("^GNU gdb \([^\)]*\)\s*([0-9\.\-a-z]*)", version_re_flags)
	, "cmake":re.compile("^cmake version ([0-9\.]*)", version_re_flags)
	, "bison":re.compile("^bison \(GNU Bison\) ([0-9\.]*)", version_re_flags)
}


qt_to_docker_platform_map={
	  "linux/amd64":"linux/x64"
	, "linux/386":"linux/x86"
}


def sigint_handler(signum, frame):
	print("\n\n<CTRL> + <C> Was pressed, killing app\n\n", end="", flush=True)
	exit(1)


signal.signal(signal.SIGINT, sigint_handler)



def prepare_docker_client():
	global _client
	if not _client:
		_client = docker.from_env()
		#_client = docker.DockerClient( base_url='unix://var/run/docker.sock' )
	return _client


def prepare_env(root_folder):
	loader = jinja2.ChoiceLoader(
		[
			jinja2.FileSystemLoader(root_folder)
#			, jinja2.FileSystemLoader(os.path.join(project_folder, os.path.dirname(template_name)))
		]
	)
	env = jinja2.Environment(loader=loader)
	env.globals['datetime'] = datetime
	#env.filters['myfilter'] = filter
	return env


def prepare_template(filename):
	try:
		env = prepare_env(TEMPLATE_DIR)
		template_string=""
		with open(filename) as f:
			template_string = f.read()
		template = env.from_string(template_string)
		template.globals["now"]=datetime.datetime.utcnow
		template.globals["tool_name"]="OctoMY™ static-qt"
		return template
	except jinja2.exceptions.TemplateSyntaxError as tse:
		logger.error(f"Template synax error in: {filename}")
		logger.exception(tse)
	except jinja2.exceptions.TemplateNotFound as tnf:
		logger.error(f"Template not found: {filename}")
		logger.exception(tnf)
		logger.info("DIR WAS:")
		b=os.path.dirname(__file__)
		for f in pathlib.Path(b).iterdir():
			logger.info(f":{f}")


def merge_dict(a, b, path=None, update=True):
	"http://stackoverflow.com/questions/7204805/python-dictionaries-of-dictionaries-merge"
	"merges b into a, returning a"
	if path is None: path = []
	if isinstance(a, str):
		if b:
			a = b
		return a
	for key in b:
		if key in a:
			if isinstance(a[key], dict) and isinstance(b[key], dict):
				merge_dict(a[key], b[key], path + [str(key)])
			elif a[key] == b[key]:
				pass # same leaf value
			elif isinstance(a[key], list) and isinstance(b[key], list):
				al=len(a[key])
				for idx, val in enumerate(b[key]):
					if al<=idx:
						a[key].append(val)
					else:
						a[key][idx] = merge_dict(a[key][idx], b[key][idx], path + [str(key), str(idx)], update=update)
			elif update:
				a[key] = b[key]
			else:
				raise Exception('Conflict at %s' % '.'.join(path + [str(key)]))
		else:
			a[key] = b[key]
	return a


def parse_config(path:pathlib.Path, variation_type, child_type):
	#logger.info(f"Parsing {path}: {variation_type} as {child_type}")
	ret = dict() #{"parsed": True}
	labels = dict()
	if variation_type is not None and path.name is not None:
		labels[variation_type] = path.name
	raw = ""
	fn = path.joinpath(child_type)
	with open(fn ,'r') as file:
		raw = file.read()
	if raw:
		j = None
		try:
			j = json.loads(raw)
		except json.decoder.JSONDecodeError as e:
			logger.warning(raw)
			logger.exception(e)
			logger.warning(fn)
			return None
		if j:
			for k,v in j.items():
				#logger.info(f"Found in config: {k}={len(str(v))}")
				ret[k] = v
	ret["labels"] = labels
	return ret


def merge_config(low, high):
	return merge_dict(dict(low), high)


def read_variations(path:pathlib.Path, variation_type=None):
	"""
	Traverse subdirectories of given path to discover and read configurations
	"""
	#logger.info(f"Iterating {p} from {variation_type}")
	files = [f for f in path.iterdir() if f.is_file()]
	if len(files) != 1:
		logger.warning(f"No child type in {path.name}")
		return None
	variation = path.name
	child_type = files[0].name
	conf = parse_config(path, variation_type, child_type)
	if not conf:
		return None
	ret = {
		  "t":variation_type
		, "v":variation
		, "c": conf
	}
	variations = list()
	for variation in [f for f in path.iterdir() if f.is_dir()]:
		#logger.info(f" + var={variation.name}")
		sub_config = read_variations(variation, child_type)
		if sub_config:
			variations.append(sub_config)
	ret["s"] = variations
	return ret


def enumerate_variations_worker_old(raw, sep="/", l=0):
	dot='.'
	dash='-'
	ret=dict()
	if raw and isinstance(raw, dict):
		variation = raw.get("variation", None)
		variation_type = raw.get("variation_type", None)##############################
		config = raw.get("config", dict())
		config["dotname"] = variation
		config["dashname"] = variation
		variations = raw.get("variations", list()) ####################################
		#logger.info(f"{'  '*l}enumerate_variations_worker({variation_type}={variation} , c={pprint.pformat(config)}, vs={len(variations)})")
		if len(variations) >0:
			for sub_name, sub in variations.items():
				parts = enumerate_variations_worker(sub, sep, l+1)
				for part_name, part_config in parts.items():
					key = f"{variation}{sep}{part_name}" if variation_type else part_name
					dot_part = part_config.get("dotname","")
					dotname = f"{variation}{dot}{dot_part}" if variation_type else dot_part ################
					dash_part=part_config.get("dashname","")
					dashname = f"{variation}{dash}{dash_part}" if variation_type else dash_part
					##################################
					if isinstance(part_config, dict): 
						part_config["dotname"] = dotname
						part_config["dashname"] = dashname
						ret[key] = merge_config(dict(config), part_config)
					else:
						config["dotname"] = dotname
						config["dashname"] = dashname
						ret[key] = config
		else:
			ret[variation] = config
	return ret


def enumerate_variations_worker(raw):
	ret = list()
	if not raw or not isinstance(raw, dict):
		return ret
	v = raw.get("v", None)
	t = raw.get("t", None)
	p = raw.get("p", list())
	c = raw.get("c", dict())
	s = raw.get("s", list())
	# Leaf
	if len(s) <= 0:
		p += [v]
		for sep in [".", "/", "_", "-"]:
			k = sep.join(p)
			c[sep] = k
		c["p"] = p
		ret.append(c)
	else:
		for sraw in s:
			# logger.info(f"sraw:\n{pprint.pformat(sraw)}")
			sraw["p"] = t and p + [v] or p
			sret = enumerate_variations_worker(sraw)
			for sc in sret:
				sc=merge_config(sc, c)
				ret.append(sc)
	return ret


def enumerate_variations(raw):
	ret = enumerate_variations_worker(raw)
	return ret


def decorate_variations(variations):
	for config in variations:
		labels = config.get("labels", dict())
		qt_version=labels.get("qt_version")
		if qt_version:
			pass
			# labels["qt_major"], labels["qt_minor"], labels["qt_patch"] = qt_version.split(".")
		else:
			labels["qt_version"] = f"{labels.get('qt_major')}.{labels.get('qt_minor')}.{labels.get('qt_patch')}"
			del labels["qt_major"]
			del labels["qt_minor"]
			del labels["qt_patch"]
		config["labels"] = labels
		config["tag"] = f"{REGISTRY_NAME}:{config.get('.')}"
		config["registry_url"] = f"https://{config.get('tag')}"
		sha_1 = hashlib.sha1()
		sha_1.update( bytes(str(config.get(".") ), encoding='utf') )
		config["id"] = sha_1.hexdigest()
		
		print(f"QT VERSION; {labels.get('qt_version')} dot: {config.get('.')} id:{config.get('id')}")
	return variations


def generate_template_file(config, filename):
	#print(f"Rendering {config.get('.')} templates:")
	template=prepare_template(filename)
	#pprint.pprint(config)
	output = template.render(config=config)
	# logger.info(name)
	# logger.info(output)
	return output


def generate_docker_files(configs):
	for config in configs:
		config["dockerfile"]=generate_template_file(config, TEMPLATE_FILE)
		config["dockerfrom"]=generate_template_file(config, FROM_FILE)
		config["dockerpull"]=generate_template_file(config, PULL_FILE)
		config["downloadospkg"]=generate_template_file(config, DOWNLOAD_PKG_FILE)
	return configs


def save_docker_files(configs, base_path):
	for config in configs:
		name = config.get(".")
		if not name:
			logger.error("Could not save dockerfile for config without name")
			continue
		dockerfile = config.get("dockerfile")
		if not dockerfile:
			logger.error("Could not save dockerfile for config '{name}' missing dockerfile")
			continue
		path = base_path.joinpath(name)
		#logger.info(f"Creating dir {path}")
		path.mkdir(parents = True, exist_ok = True)
		file_path = path.joinpath( "Dockerfile")
		#logger.info(f"Saving to {file_path}")
		config["dockerfile_dir"] = str(path)
		config["dockerfile_path"] = str(file_path)
		with open(file_path, "w") as file:
			file.write(dockerfile)

def filter_bad_packages(stream):
	not_found = not_found_re.findall(stream)
	no_candidate = no_candidate_re.findall(stream)
	return [*not_found, *no_candidate]


def pretty_print_list(l):
	l.sort()
	for v in l:
		print(f"\t, \"{v}\"")


def handle_build_error(build_error, config):
	print("")
	print("---- handle_build_error START")
	stream=""
	error_details=[]
	for chunk in build_error.build_log:
		stream_part=chunk.get("stream")
		if stream_part:
			stream+=stream_part
		error_detail_part=chunk.get("errorDetail")
		if error_detail_part:
			error_details.append(error_detail_part)
	print("---- ORIGINAL DOCKERFILE:")
	print(config.get("dockerfile"))
	print("---- STREAM:")
	print(stream)
	bad_packages=filter_bad_packages(stream)
	print("---- BAD PACKAGES:")
	pretty_print_list(bad_packages)
	print("---- GOOD PACKAGES:")
	good_packages=[x for x in config.get("packages",[]) if x not in bad_packages]
	pretty_print_list(good_packages)
	print("---- DETAILS:")
	for detail in error_details:
		print(f"ERROR {detail.get('code', 'No code')}:")
		print(detail.get("message", "No message"))
	print("---- handle_build_error END")
	print("")


def build_docker_file(config):
	dotname = config.get(".")
	tag = config.get("tag")
	print(f"Building tag '{tag}'")
	image_labels = dict()
	for key, val in config.get("labels", dict()).items():
		image_labels[IMAGE_NAME+"."+key]=val
	try:
		client = prepare_docker_client()
		image, build_log = client.images.build(
			  path = config.get("dockerfile_dir")
			, dockerfile="Dockerfile"
			, tag = tag
			, rm = False
			, pull = True
			, labels = image_labels
			)
		config["id"] = image.id.replace(ID_PREFIX, "")
		config["short_id"] = image.short_id.replace(ID_PREFIX, "")
	except docker.errors.DockerException as de:
		logger.error("Docker error")
		logger.exception(de)
		return False
	except docker.errors.BuildError as be:
		#logger.exception("Build error")
		handle_build_error(be, config)
		return False
	return config


def build_docker_files(configs, end_on_error = True, end_after_one = True):
	done = False
	for config in configs:
		name = config.get(".")
		if not name:
			logger.error("No name for config")
			continue
		if done:
			logger.warning("Terminating on error as end-on-error is set")
			break
		if end_after_one:
			done = True
		maybe = build_docker_file(config)
		if not maybe:
			if end_on_error:
				done = True
		else:
			config = maybe
	return configs


def list_docker_images():
	client = prepare_docker_client()
	images=client.images.list()
	ret=list()
	for image in images:
		is_octomy_image=False
		for tag in image.tags:
			if tag.startswith(REGISTRY_NAME):
				is_octomy_image=True
		if is_octomy_image:
			ret.append(image)
			print(f"{image.id}:")
			for tag in image.tags:
				print(f" + {tag}")
	return ret


def get_docker_images():
	client = prepare_docker_client()
	images=client.images.list()
	ret=dict()
	for image in images:
		is_octomy_image=False
		for tag in image.tags:
			if tag.startswith(REGISTRY_NAME):
				is_octomy_image=True
		if is_octomy_image:
			ret[image.id]=image
	return ret


def remove_docker_container(config):
	client = prepare_docker_client()
	dotname=config.get("dotname")
	container_name=f"{FINGERPRINT_CONTAINER_PREFIX}{dotname}"
	try:
		container=client.containers.get(container_name)
		print(container)
		if container:
			logger.info(f"Terminating existing container '{container_name}' with timeout {KILL_TIMEOUT}:")
			container.stop(timeout=KILL_TIMEOUT)
			container.remove(force=True)
		else:
			logger.info(f"No existing container found for '{container_name}'")
	except docker.errors.NotFound as nf:
		return False
	return True


def create_docker_container(config):
	dotname=config.get("dotname")
	image= f"{REGISTRY_NAME}:{dotname}"
	container_name=f"{FINGERPRINT_CONTAINER_PREFIX}{dotname}"
	logger.info(f"Creating container '{container_name}' from iamge '{image}'")
	id=config.get("short_id")
	if not id:
		logger.warning(f"No ID found for '{image}'")
		pprint.pprint(config)
		return False
	print(f"ID={id}")
	fingerprint_script=config.get("fingerprint_script")
#    fingerprint_script=["/bin/bash", "-c", "echo fingerprint; /bin/cat /etc/os-release; /usr/bin/make --version; /usr/bin/git --version; /usr/bin/g++ --version; /usr/bin/clang --version; /usr/bin/python --version; /usr/bin/gdb --version; /usr/bin/cmake --version;    /usr/bin/bison --version; true" ]
	if not fingerprint_script:
		logger.warning(f"No fingerprint script found for '{image}'")
		pprint.pprint(config)
		return False
	print(f"fingerprint_script={fingerprint_script}")
	client=prepare_docker_client()
	container=None
	try:
		container=client.containers.create(
			  image=      id
#            , entrypoint= "" # Override entrypoint for testing TODO:REMOVE
#            , entrypoint= ["/bin/bash", "-c"]
			, command=    fingerprint_script
#            , auto_remove=True
			, name=       container_name
			)
		print(container)
	except docker.errors.NotFound as nf:
		logger.warning(f"Image '{image}' not found while creating container for fingerprint, skipping...")
		#pprint.pprint(config)
		pprint.pprint(nf)
		return False
	if not container:
		logger.warning(f"No container created for '{image}', skipping...")
		return False
	return container

def parse_fingerprint_versions(fingerprint):
	versions = dict()
	for key, re in version_re.items():
		matches=re.findall(fingerprint)
		#print(f"{key}")
		for match in matches:
			versions[key]=match;
			break;
			#print(f" + {pprint.pformat(match)}")
	return versions

def fingerprint_docker_image(config):
	dotname=config.get("dotname")
	image= f"{REGISTRY_NAME}:{dotname}"
	container_name=f"{FINGERPRINT_CONTAINER_PREFIX}{dotname}"
	id=config.get("short_id")
	if not id:
		logger.warning(f"No ID found for '{image}'")
		pprint.pprint(config)
		return False
	print(f"ID={id}")
	remove_docker_container(config)
	container=create_docker_container(config)
	if not container:
		return False
	output=False
	try:
		logger.info(f"Starting '{container.name}' to generate fingerprint")
		container.start()
		start=datetime.datetime.utcnow()
		logger.info(f"Waiting for'{container.name}' to finish...")
		container.wait()
		output=container.logs().decode("utf-8")
		interval=datetime.datetime.utcnow()-start
		logger.info(f"Fingerprint for '{image}' after {interval}:")
		print(output)
	except docker.errors.NotFound as nf:
		logger.warning(f"Image '{image}' not found while running container for fingerprint, skipping...")
		#pprint.pprint(config)
		pprint.pprint(nf)
		return False
	return output


def fingerprint_docker_images(configs, end_on_error=True, end_after_one=True):
	done=False
	for name, config in configs.items():
		dotname=config.get("dotname")
		maybe = fingerprint_docker_image(config)
		if False is maybe and end_on_error:
			done=True
		else:
			configs[name]["fingerprint_versions"] = parse_fingerprint_versions(maybe)
		if end_after_one:
			done=True
		if done:
			logger.warning("Terminating for end_on_error={end_on_error}, end_after_one={end_after_one}")
			break


def run():
	# O/S: {{ "%-55s" | format(f"{os}({os_dist}-{os_version}",) }}       #
	logger.info("############# READ")
	configs = read_variations(path = pathlib.Path(VARIATIONS_DIR))
	
	
	logger.info("############# ENURMERATE")
	configs = enumerate_variations(configs)
	
	logger.info("############# DECORATE")
	configs = decorate_variations(configs)
	
	for name, config in configs.items():
		print(config.get("dotname"))
		print(config.get("fingerprint_script"))
		break;
	
	if True:
	
		logger.info("############# GENERATE")
		configs = generate_docker_files(configs)
	
		for _, config in configs.items():
			print(config.get("dockerfile"))
			break
	
		logger.info("############# SAVE")
		save_docker_files(configs, pathlib.Path("./output/"))
	
		logger.info("############# BUILD")
		build_docker_files(configs)
	
	
	logger.info("############# LIST")
	list_docker_images()
	logger.info("############# FINGERPRINT")
	fingerprint_docker_images(configs)


class QtBuilder:
	def __init__(self):
		self.configs_by_id=dict()
		pass
	
	def setup(self, sec=5):
		self.configs = read_variations(path = pathlib.Path(VARIATIONS_DIR))
		#logger.info(f"INTERMEDIATE1: \n{pprint.pformat(self.configs)}")
		self.configs = enumerate_variations(self.configs)
		#logger.info(f"INTERMEDIATE2: \n{pprint.pformat(self.configs)}")
		self.configs = decorate_variations(self.configs)
		self.configs = generate_docker_files(self.configs)
		save_docker_files(self.configs, pathlib.Path("./output/"))
		print(f"Waiting {sec} sec for docker to settle...")
		time.sleep(sec)
		print(f"Continuing!")
		for config in self.configs:
			self.configs_by_id[config.get("id")]=config
		build_docker_files(self.configs)
		
	def build(self):
		print("Build not implemented")

	def get_build_by_id(self, id:str):
		return self.configs_by_id.get(id)

	def list_docker_images(self):
		return list_docker_images()
	
	def get_docker_images(self):
		return get_docker_images()
	
	def get_configs(self):
		return self.configs_by_id
