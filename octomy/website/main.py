from .context import Context, get_context, prepare_templates
from .routes.qtbuilder import qtbuilder_route
#from octomy.web.autoroute import AutoRouter
from .tools.autoroute import AutoRouter
from .tools.qtbuilder import QtBuilder
from .tools.search import SearchEngine
from fastapi import Depends, FastAPI, Request, HTTPException
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from octomy.log import setup_logging

from multiprocessing import Pool
from typing_extensions import Annotated
import datetime
import os
import pprint

logger = setup_logging(__name__)

context = get_context()

app = FastAPI()

app.include_router(qtbuilder_route)


templates = prepare_templates()


ar = AutoRouter(app, context)

# Static resources such as logos, icons and stylesheets
try:
	app.mount("/static", StaticFiles(directory=f"{context.webroot}/static"), name="static")
except Exception as e:
	logger.info("No static folder found, skipping...")



# Website root, generic landing page
@app.get("/", response_class=HTMLResponse)
async def website_root(request: Request, context: Annotated[Context, Depends(get_context)]):
	global templates
	return templates.TemplateResponse("index.html", {"request": request, "title": "OctoMY&trade; - Ready-to-run robot software."})


# Website search
@app.get("/search/json", response_class=JSONResponse)
async def website_search(request: Request, q:str, context: Annotated[Context, Depends(get_context)]):
	if not context.get("search_engine"):
		raise HTTPException(status_code=404)
	q = q.strip()
	ret = se.federated_search(q)
	return out

