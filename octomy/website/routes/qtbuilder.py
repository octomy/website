from ..context import Context, get_context, prepare_templates
#from aiopipe import aiopipe
from contextlib import contextmanager
from fastapi import APIRouter, Depends, Request, HTTPException, status, WebSocket, WebSocketDisconnect
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from functools import lru_cache
from multiprocessing import Process
from typing_extensions import Annotated
import asyncio
import concurrent.futures
import functools
import glob
import inspect
import io
import logging
import os
import pprint
import random
import sys
import time
import traceback



logger = logging.getLogger(__name__)


qtbuilder_route = APIRouter(
	prefix="/build/qt",
	tags=["items"],
	#dependencies=[Depends(get_token_header)],
	responses={404: {"description": "Not found"}},
)

templates = prepare_templates()


#####################################################################


@contextmanager
def redirect_std(out_stream=None, err_stream=None):
	"""
	Context to capture stdout and/or stderr in given streams
	"""
	old_stdout = sys.stdout
	old_stderr = sys.stderr
	if out_stream:
		sys.stdout = out_stream
	if err_stream:
		sys.stderr = err_stream
	try:
		yield
	finally:
		sys.stdout = old_stdout
		sys.stderr = old_stderr

def dummy_blocking_task():
	sec=10
	ct=sec
	id=random.randint(1,1000)
	print(f"#{id} Doing blocking task for {sec} seconds")
	while True:
		time.sleep(0.5)
		print(f"#{id} Dummy stdout {ct}\n")
		time.sleep(0.5)
		ct -= 1
		if ct<=0:
			break
	print(f"#{id} Dummy stderr {ct}\n", file=sys.stderr) 
	print(f"#{id} Blocking task finished after {sec} seconds")
	return id


class file_like:
	def __init__(self, handler, arg = None, loop = None):
		self.handler = handler
		self.loop = loop
		self.arg = arg

	def write(self, line):
		if line:
			if not self.handler:
				logger.warning(f"Unhandled {self.arg}: '{line}'")
				return
			if inspect.iscoroutinefunction(self.handler):
				if self.loop:
					task = self.loop.create_task(self.handler(line, self.arg))
				else:
					logger.error(f"Not able to handle {self.arg}, missing loop: '{line}'")
			else:
				self.handler(line, self.arg)


async def send(websocket, message:str, type:str="rx"):
	return await websocket.send_json({"msg":message, "type":type})

# Wrap in async
async def async_task(websocket, callback):
	# (ab)use python's "file_like" way of implementing streams
	async def line_handler(line, arg=None):
		await send(websocket, message=line, type=arg or "rx")
	loop = asyncio.get_running_loop()
	out_stream = file_like(handler = line_handler, arg = "rx", loop = loop)
	err_stream = file_like(handler = line_handler, arg = "rxe", loop = loop)
	ret = None
	loop = asyncio.get_running_loop()
	# Redirect stdout&stderr to streams
	with redirect_std(out_stream, err_stream):
		# Run the blocking function
		ret = await loop.run_in_executor(None, lambda: callback())
	return ret


######################################################################

@qtbuilder_route.get("/wss")
async def get_wss(request: Request):
	return templates.TemplateResponse(f"{qtbuilder_route.prefix}/log.html", {"request": request})




class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocket):
        self.active_connections.remove(websocket)

    async def send(self, message: str, websocket: WebSocket):
        await send(websocket, message)

    async def broadcast(self, message: str):
        for connection in self.active_connections:
            await connection.send_text(message)


async def list_out(websocket, list_name, items):
	await send(websocket, f"Listing {list_name}:")
	for name, item in items:
		await send(websocket, f" + {name}: {pprint.pformat(item)}")
	

manager = ConnectionManager()

async def sendHelp(websocket, parts):
	if len(parts) <= 1:
		await send(websocket, """
Server side commands:

+ help <topic>  Show help for the given topic such as a command
+ test          Start a dummy background process for debugging purposes
+ config        Start qt builds configuration process
+ disconnect    disconnect websocket from server side
+ list <type>   List available items of given type
+ bob           Your mum
	""")

	else:
		argument = parts[1]
		if argument == "list":
			await send(websocket, """
List help:

+ list configs        List alle the detected configurations
+ list docker_images  List alle the detected docker images
	""")



def help_list():
	logger.info(inspect.stack()[0][3])

def help_show():
	logger.info(inspect.stack()[0][3])

def help():
	logger.info(inspect.stack()[0][3])

def list_configs():
	logger.info(inspect.stack()[0][3])

def show_config(config):
	logger.info(inspect.stack()[0][3])

def list_docker_images():
	logger.info(inspect.stack()[0][3])

def show_docker_image(docker_image):
	logger.info(inspect.stack()[0][3])


map = {
	"help":{
		"list": {"f":help_list, "a":0}
		, "show": {"f":help_show, "a":0}
		, "": {"f":help, "a":0}
	}
	, "list":{
		"configs": {"f":list_configs, "a":0}
		,"docker_images": {"f":list_docker_images, "a":0}
		, "f":help_list
		, "a":0
	}
	, "show":{
		"config": {"f":show_config, "a":1}
		,"docker_image": {"f":show_docker_image, "a":1}
	}
}

def parse_command(data):
	parts = data.split()
	f = None
	a = None
	h = None
	current_level = map
	tot = len(parts)
	left = tot
	for part in parts:
		left -= 1
		num = tot - left
		if part == "a" or part == "f":
			break
		logger.info(f"inspecting part {num}/{tot} {part}")
		level = current_level.get(part)
		if not level:
			logger.info(f"no level {pprint.pformat(level)}")
			current_level=None
			break
		h = " ".join((h or "").split() + [part])
		current_level = level
	if current_level:
		logger.info(f"current_level {pprint.pformat(current_level)}")
		final = current_level.get("")
		if final:
			f = final.get("f")
			act = final.get("a")
	return f, a, h


@qtbuilder_route.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket, context: Annotated[Context, Depends(get_context)]):
	global building
	loop = asyncio.get_running_loop()
	await manager.connect(websocket)
	try:
		while True:
			data = (await websocket.receive_text()).strip()
			if data:
				logger.info(f"WS got: {data}")
				f, a, h = parse_command(data)
				logger.info(f"PARSED AS: f='{pprint.pformat(f)}', a='{pprint.pformat(a)}', h='{pprint.pformat(h)}'")
				if f:
					f()
				continue
				action = parts[0]
				if action == "help":
					await sendHelp(websocket, parts)
				elif action == "test":
					await send(websocket, f"Dummy running...")
					ret = await async_task(websocket=websocket, callback=dummy_blocking_task)
					await send(websocket, f"Dummy ended with {ret}")
				elif action == "setup":
					ret = await async_task(websocket=websocket, callback=context.qt_builder.setup)
				elif action == "bob":
					await send(websocket, f"Your mum")
				elif action == "disconnect":
					await send(websocket, f"Server asked to disconnect...")
					await websocket.close()
				elif action == "show":
					if len(parts) < 2:
						await send(websocket, f"Please specify what you want to show")
						await sendHelp(websocket, parts)
					else:
						argument = parts[1]
						if argument == "config":
							items = context.qt_builder.get_configs().items()
						elif argument == "docker_images":
							items = context.qt_builder.get_docker_images().items()
						else:
							await send(websocket, f"Unknown item type to list: '{argument}'. Try 'help'.")
							await sendHelp(websocket, parts)
							continue
						if items:
							await list_out(websocket, argument, items)
						else:
							await send(websocket, f"No items to list for '{argument}'")

				elif action == "list":
					if len(parts) < 2:
						await send(websocket, f"Please specify what you want to list")
						await sendHelp(websocket, parts)
					else:
						argument = parts[1]
						items = None
						if argument == "configs":
							items = context.qt_builder.get_configs().items()
						elif argument == "docker_images":
							items = context.qt_builder.get_docker_images().items()
						else:
							await send(websocket, f"Unknown item type to list: '{argument}'. Try 'help'.")
							await sendHelp(websocket, parts)
							continue
						if items:
							await list_out(websocket, argument, items)
						else:
							await send(websocket, f"No items to list for '{argument}'")
				else:
					await send(websocket, f"Received unknown command: {pprint.pformat(data)}. Try 'help'.")
	except WebSocketDisconnect:
			manager.disconnect(websocket)
	except Exception as e:
			await send(websocket, f"Unknown error: {pprint.pformat(e)}")

@qtbuilder_route.get("/{id}")
async def get_qt_build(request: Request, id:str, context: Annotated[Context, Depends(get_context)]):
	global templates
	logger.info(f"################### build {id} ########################{__file__}")
	# for filename in glob.iglob('/app/**/*.html', recursive=True):
	# 	 logger.info(f" + {filename}")
	build = context.qt_builder.get_build_by_id(id)
	if not build:
		raise HTTPException(status_code=404)
	return templates.TemplateResponse(f"{qtbuilder_route.prefix}/build.html", {"request": request, "title": "Data", "id":id, "build":build})
